# Xumak internal employee requests

There is an initiative to develop an application where employees can submit their petitions.
The proposed solution would use a BPM service where a developer designs the initial workflows for the corresponding requests and also would provide the corresponding API; Jahia is the proposed interface for the users.

After some efforts testing available options of BPM systems - we chose [kissflow](http://www.kissflow.com) - this service provides almost all the needed requirements, even though, because some benefits we want to take - the procedures will be done on both systems: some on kissflow and others on jahia.

Find corresponding documentation for kissflow and jahia - in this folder.
