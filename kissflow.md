# Kissflow (xumakero.com)

**Overview**

Kissflow is a Workflow Management Software used to manage internal employee requests in Xumak.

This is kissflow [main site](https://kissflow.com/)

This tool allows to define process workflows named as Apps, then users can create requests using this Apps to follow the process defined in the workflow.

Kissflow is a web application, a user is needed to access [here](https://app.kissflow.com/)

It also provides RESTful Endpoints, this is how Xumakero is intended to connect with kissflow. For techinal information for this API click [here](https://help.kissflow.com/tips-and-tricks/api-documentation/rest-api-points)

The Kissflow API Java Code is part of the main Xumakero Jahia Project following this path: xumakHr/xumakhr-components-module/src/main/java/com/xumak/kissflowapi/

**Request Creation and Follow Up Process Basic Diagram**

![xumakHr Components module](/xumakero-kissflow-workflow.png)

**Request Creation**

The employees create requests using the Jahia App named Xumakero. Internally, Xumakero creates the kissflow requests using the kissflow RESTful API with his Project Manager kissflow user account, this is to reduce the number of licenses needed.

**Request Follow Up (Employee)**

To see the status of their requests, the emloyees have to use Xumakero connecting internally to kissflow as mentioned before.

**Request Follow Up (Staff)**

The staff team has the resposability to follow up the request created, they recive notifications by email and have user accounts directly in the Kissflow Web Application, here they can consult their pending requests for approval, where thay can approve or deny the different requests.






