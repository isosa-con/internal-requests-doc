# Jahia (xumakero.com)
**Overview** of what we have being doing: after the original intranet got discontinued - someone created a static [site](http://hr.xumak.gt/) - that is how employees have been submiting their requests.
A couple of months ago we started moving these google forms to jahia and in parallel other person had been doing research about BPM options; the static page can now be generated inside jahia but it has never be put on production - instead of that, we now have instructions to integrate these requests forms on (kissflow/jahia).

For **technical aspects of kissflow** refer to corresponding file on this folder.

### Procedure to be done on jahia
The actual web site will be developed in jahia; in addition, the java API to communicate with kissflow.
To have a better map of the structure - here is a list of the sources:

Components module | Templateset module
----------------- | ------------------
![xumakHr Components module](/components-module.png) | ![xumakHr Components module](/templateSet-module.png)

**¿Two java projects?**: as jahia developers we should now that a recommended approach for a site is having two *modules*: templateset & default module.


### Requirements recollected
These have come up from the meetings *plus our own ideas.*

![xumakHr Components module](/xumakero.png)

### To start developing on this project
*List of the things needed*:
* google API for javascript [sign-in button](https://developers.google.com/identity/sign-in/web/sign-in)
* java
* junit
* maven
* git
* github or gitlab
* jahia
  * xumakHr modules
  * CI infrastructure (XMS department)
  * jenkins
* jcr api knowledge
* angularjs/javascript
* kissflow
